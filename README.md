#Service for Page Hyperlinks Analysis Including Their Availability by URL Address#
*Academic Supervisor: Victor Dudarev*

The service is intended to search for invalid hyperlinks on a HTML web page, 
identify inaccessible resource files from the HTML code of the page, 
and from imported and embedded CSS stylesheets. 
The recursive search for hyperlinks in CSS files is implemented.

Demo: [hyperlinks.ntarasov.ru](http://hyperlinks.ntarasov.ru/)


*E-mail: [mail@ntarasov.ru](mailto:mail@ntarasov.ru)*