function changeInputLinkTableState(visibility)
{
    var stateString = "hidden";
    if (visibility)
        stateString = "visible";
    $("#inputLinkResult").css("visibility", stateString);
}

function generateResults()
{
    if (typeof getLinkResult.Error != "undefined") {
        generateErrorInputLinkTable(getLinkResult.Error);
        changeInputLinkTableState(true);
        return;
    }
    if (typeof urlList != "object" || typeof urlList == "undefined") {
        generateErrorInputLinkTable(null);
        changeInputLinkTableState(true);
        return;
    }
    $("#linkResultContent").html("<font id='requestedUrl'>" + URL + "</font> <br/>");
    var loadingGif = "<img src='resources/img/ripple.gif' height='30px'/>";
    $("#linkResultContent").append("<font id='requestedUrlStatus'>" + loadingGif + "</font> <br/>");
    updateSourceUrlStatusString('#requestedUrlStatus', URL);
    $("#linkResultContent").append("<font id='requestedLinkOtherInfo'>Гиперссылок найдено: " + urlList.length + "</font><br/>");
    if (headOnly) {
        $("#linkResultContent").append("<span style='font-size: 8; font-color: #555'>Запросы HEAD вкл.</span><br/>");
    }
    $("#linkResultContent").append("<table><tr><td><span id='stats'></span></td>" +
        "<td id='processedCount'><span></span><span><span></td></tr></table><br/>");
    $("#inputLinkResult").css("margin-top", $(window).height() / 2 - $("#inputLinkResult").height() / 2);
    changeInputLinkTableState(true);
}

function updateSourceUrlStatusString(selector, url) {
    if (headOnly) {
        var dataToSend = { "url": url, "head": 1 };
    } else {
        var dataToSend = { "url": url };
    }
    $.ajax({
        url: "Ping.aspx",
        type: "POST",
        data: dataToSend,
        dataType: "json",
        success: function (data) {
            $(selector).html(data.Status + " / " + data.StatusText);
            if (data.Status >= 300 && data.Status < 400) {
                $(selector).append("<br/><font color='#555' style='font-size: 12pt' id='newUrlValue'></font>");
                updateNewUrlValue("#newUrlValue", url);
            }
            $(selector).css('color', getColorByStatusCode(data.Status));
        },
        error: function () {
            $(selector).html("Не могу получить код ответа :(");
            $(selector).css('color', getColorByStatusCode(404));
        }
    });
}

function updateNewUrlValue(viewSelector, oldUrl) {
    $.ajax({
        url: "GetTargetAddress.aspx",
        type: "POST",
        data: { "url":  oldUrl},
        dataType: "json",
        success: function (data) {
            if (typeof data.Target != "undefined") {
                URL = data.Target;
                $(viewSelector).html("На адрес " + data.Target);
            }
        }
    });
}

function generateErrorInputLinkTable(errorText)
{
    $("#linkResultContent").html("<font id='requestedUrl'>" + URL + "</font> <br/>");
    $("#linkResultContent").append("<font id='requestedUrlStatusError'>Error</font> <br/>");
    $("#linkResultContent").append("<font id='requestedLinkOtherInfoError'>" + errorText + "</font>");
    $("#inputLinkResult").css("margin-top", $(window).height() / 2 - $("#inputLinkResult").height() / 2);
    switchPingState("rm");
}

function addHomeLink() {
    var htm = "<div id='homeLink'><a href='/'>В начало</a></div>";
    $("body").append(htm);
}