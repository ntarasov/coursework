﻿$(document).ready(function () {
    var htmlContent = $("#resultsDiv_sample").html();
    var htmlContentSize = $("#resultsDiv_sample_size").html();
    $("#resultsDiv_internalhyperlinks").html(htmlContent);
    $("#resultsDiv_internalresources").html(htmlContentSize);

    $("#resultsDiv_externalhyperlinks").html(htmlContent);
    $("#resultsDiv_externalresources").html(htmlContentSize);
});

function fixLink(url, viewSelector) {
    var fixedUrl = url;
    if (url.indexOf('http') != 0) {
        fixedUrl = "http://" + url;
        $(viewSelector).val(fixedUrl);
    }
    return fixedUrl;
}

function getLinks(url) {
    var fixedLink = fixLink(url, "#url");
    if (fixedLink !== url) {
        setTimeout(function() {
            getLinks(fixedLink);
        }, 700); // Animate fixing
        return;
    }
    if ($("#headOnlyCheckbox").is(':checked'))
        headOnly = true;
    $("#wrapper").remove();
    if (loaded)
        return; // TO DO
    URL = url;
    changeLoadingGifState(true);
    $.ajax({
        url: "GetLinks.aspx",
        type: "POST",
        data: { "url": url },
        dataType: "json",
        success: function (data) {
            getLinkResult = data;
            loaded = true;
            changeLoadingGifState(false);
            urlList = data.Result;
            generateResults();
            addHomeLink();
        },
        error: function (ex, status, errorThrown) {
            generateErrorInputLinkTable(status);
        }
    });
}

var finishedThreads = 0;
function makeNextPingQuery(prevNumber, threadNumber)
{
    var curr = prevNumber + 1;
    if (curr == urlList.length)
    {
        ++finishedThreads;
        if (finishedThreads == threads) {
            onVeryEnd();
            switchPingState(false);
            onFinish();
        }
        return;
    }
    appendResults(curr, urlList[curr], threadNumber, urlList[curr].Category);
    if (curr > lastProcessed)
        lastProcessed = curr;
    if (headOnly) {
        var dataToSend = { "url": urlList[curr].Url, "head": 1 };
    } else {
        var dataToSend = { "url": urlList[curr].Url };
    }
    $.ajax({
        url: "Ping.aspx",
        type: "POST",
        data: dataToSend,
        dataType: "json",
        success: function (data) {
            if (typeof data.Status == "undefined" || data.status === "undefined") {
                removeLastResult(threadNumber);
                makeNextPingQuery(prevNumber, threadNumber);
                return;
            }
            completeLastResult(data, threadNumber, urlList[curr].Category);
            if (!pause)
                makeNextPingQuery(lastProcessed, threadNumber);
        },
        error: function () {
            alert("Не могу выполнить Ping запрос по адресу " + urlList[curr].Url + ".\n" +
                "Подробности в JS логе браузера");
            console.log("Ping error\nURL: " + urlList[curr].Url + "\nCategory: " + urlList[curr]);
        }
    });
}

function changeLoadingGifState(state)
{
    var cssState = "visible";
    if (!state) {
        $("#loadingDiv").remove();
        return;
    }
    $("#loadingGif").css("visibility", cssState);
}

var firstStart = true;
function switchPingState(status) {
    if (status === "rm") {
        $("#switch").remove();
        return;
    }
    if (firstStart)
    {
        firstStart = false;
        $("#inputLinkResult").animate({
            marginTop: "50px"
        }, 400, function () { switchPingState(true) });
        return;
    }
    var switchText;
    if (typeof status != "undefined")
        pause = status;
    if (!pause) {
        pause = true;
        switchText = "play.png";
    }
    else {
        pause = false;
        for (var i = 0; i < threads; ++i)
            makeNextPingQuery(lastProcessed, i);
        changeResultsTableState(true);
        switchText = "pause.png";
    }
    $("#switch > img").attr("src", "/resources/img/" + switchText);
}

function onFinish() {
    $("#switch img").attr("src", "/resources/img/complete.png");
    $("#switch").off();
}

function onVeryEnd() {
    var showingState = {
        "eh": hideTableIfEmpty("#resultsDiv_externalhyperlinks", 3),
        "er": hideTableIfEmpty("#resultsDiv_externalresources", 2),
        "ih": hideTableIfEmpty("#resultsDiv_internalhyperlinks", 1),
        "ir": hideTableIfEmpty("#resultsDiv_internalresources", 0)
    }

    sortTable("#resultsDiv_externalhyperlinks");
    sortTable("#resultsDiv_externalresources");
    sortTable("#resultsDiv_internalhyperlinks");
    sortTable("#resultsDiv_internalresources");

    export_showCsvLink(showingState);
}

function hideTableIfEmpty(selector, tablePos) {
    if (isEmptyString($(selector + " > .resultsTable > tbody").html())) {
        $(selector).remove();
        $(".resultsHeader:eq(" + tablePos + ")").remove();
        return false;
    }
    return true;
}

function isEmptyString(str) {
    for (var i = 0; i < str.length; ++i)
        if (str[i] != ' ' && str[i] != '\n' && str[i] != '\r')
            return false;
    return true;
}


function sortTable(selector) {
    selector += " > .resultsTable > tbody";
    children = selector + " > tr";
    var data = [], i=0;
    $(children).each(function() {
        data[i] = {
            Class: this.className,
            Content: this.innerHTML,
            ErrorCode: $(this).children('td:eq(2)').text()
        }
        ++i;
    });
    data.sort(function(a, b) {
        return b.ErrorCode - a.ErrorCode;
    });
    $(selector).html("");
    var length = i;
    for (i = 0; i < length; ++i) {
        var pos = data[i].Content.indexOf("<b>");
        pos += 3;
        while (data[i].Content[pos] >= '0' && data[i].Content[pos] <= '9') {
            ++pos;
        }
        data[i].Content = data[i].Content.substr(pos, data[i].Content.length);
        $(selector).append("<tr height='50px' class='" + data[i].Class + "'><td><b>" + (i+1) + data[i].Content + "</tr>");
    }
}