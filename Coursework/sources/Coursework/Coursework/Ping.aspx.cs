﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Coursework.Classes;
using Coursework.Classes.Ping;
using Coursework.Tools;
using Coursework.Tools.JsonTypes;

namespace Coursework
{
    public class Ping : Page
    {
        /// <summary>
        /// Метод, вызываемый при обращении к странице
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "application/json";
            bool headOnly = false;
            string url = Request.Form["url"],
                headOnlyParam = Request.Form["head"];
            if (url == null)
            {
                url = Request.QueryString["url"];
                headOnlyParam = Request.QueryString["head"];
                if (url == null)
                {
                    Response.Write("{}");
                    return;
                }
            }
            headOnly = headOnlyParam != null;
            try
            {
                PingRequest request = new PingRequest(url);
                PingResponse response = request.Ping(headOnly);
                JsonPingResult jsonResult = new JsonPingResult(response);
                Response.Write(JsonTools.MakeJsonString(jsonResult));
            }
            catch (Exception ex)
            {
                JsonError error = new JsonError(ex);
                Response.Write(JsonTools.MakeJsonString(error));
            }
        }
    }
}