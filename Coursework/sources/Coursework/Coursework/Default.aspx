﻿<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <script src="js/jquery-1.12.0.min.js"></script>
        <script src="js/global.js"></script>
        <script src="js/inputLinkResult.js"></script>
        <script src="js/csvExport.js"></script>
        <script src="js/results.js"></script>
        <script src="js/main.js"></script>
        <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css" />
        <link rel="stylesheet" href="css/Default.css" />
        <style>

        </style>
        <title>Анализ гиперссылок на странице</title>
    </head>
    <body>
        <div id="wrapper">
            <div class="centered">
                <h3>Анализ гиперссылок на странице</h3>
                <i><span style="font-size: 10pt; color: #555" id="serviceInfo">
                   Для начала анализа введите полный адрес страницы в форму
                </span></i>
                <br/>
                <br/>
                <input type="text" id="url" placeholder="URL" value="http://vk.com/" class="form-control"/><br />
                <center>
                    <input type="button" onclick="getLinks($('#url').val())" value="Загрузить" class="btn btn-primary" /> <br />
                </center><br/>
                <input type="checkbox" id="headOnlyCheckbox"/> 
                <span style="font-size: 10pt">Запрашивать только заголовки (HEAD)</span>
                <br/>
                <br/>
                <span style="font-size: 10pt">
                     Сервис предоставляет возможность проверить работоспособность 
                    ссылок и доступность ресурсов на HTML веб-странице. Поддерживаются протоколы HTTP(S). 
                    Присутствует рекурсивный обход CSS таблиц стилей для поиска ресурсов, загружаемых браузером.
                </span>
                <br/>
                <br/>
                <span style="font-size: 8pt; color: #555">
                    Используется User-Agent<br/>
                    Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.70 Safari/533.4
                </span>
            </div>
        </div>
        <div id="loadingDiv">
            <img src="resources/img/hourglass.gif" id="loadingGif"/>
        </div>
        <div id="inputLinkResult" class="resultsDiv">
            <table class="table">
                <tbody>
                    <tr>
                        <td class="noborder">
                            <center>
                                <img src="resources/img/file.svg" height="60" id="linkResultImage" />
                            </center>
                        </td>
                        <td id="linkResultContent" class="noborder">

                        </td>
                        <td class="noborder">
                            <a onclick="switchPingState()" id="switch">
                                <img src="resources/img/play.png" />
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        
        <center><h4 class="resultsHeader">Внутренние файлы ресурсов</h4></center>
        <div id="resultsDiv_internalresources" class="resultsDiv">
        </div>
        
        <center><h4 class="resultsHeader">Внутренние ссылки</h4></center>
        <div id="resultsDiv_internalhyperlinks" class="resultsDiv">
        </div>
        
        <center><h4 class="resultsHeader">Внешние файлы ресурсов</h4></center>
        <div id="resultsDiv_externalresources" class="resultsDiv">
        </div>
        
        <center><h4 class="resultsHeader">Внешние ссылки</h4></center>
        <div id="resultsDiv_externalhyperlinks" class="resultsDiv">
        </div>

        <div id="resultsDiv_sample" class="invisible">
              <table class="table resultsTable">
                    <thead>
                        <th>#</th>
                        <th>URL</th>
                        <th>Код ответа HTTP</th>
                        <th>Статус</th>
                        <th>MIME тип контента</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
       </div>
       <div id="resultsDiv_sample_size" class="invisible">
              <table class="table resultsTable">
                    <thead>
                        <th>#</th>
                        <th>URL</th>
                        <th>Код ответа HTTP</th>
                        <th>Статус</th>
                        <th>MIME тип контента</th>
                        <th>Размер</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
        </div>
    </body>
</html>