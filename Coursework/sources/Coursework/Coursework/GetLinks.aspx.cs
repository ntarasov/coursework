﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Linq;
using System.Web;
using Coursework.Classes;
using System.Net;
using Coursework.Tools;
using Coursework.Tools.JsonTypes;

namespace Coursework
{
    public class GetLinks : Page
    {
        /// <summary>
        /// Метод, вызываемый при обращении к странице
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "application/json";
            string url = Request.Form["url"];
            if (url == null)
            {
                url = Request.QueryString["url"];
                if (url == null)
                {
                    Response.Write("{}");
                    return;
                }
            }
            try
            {
                ExtendedWebPage page = new ExtendedWebPage(url);
                page.Download();
                
                // FOR DEBUG PURPOSES ONLY:
                if (Request.QueryString["debug"] == "1")
                    Response.Write("<!--" + page.Content + "-->\n");

                Link[] arr = page.ParseLinks()
                    .Distinct(new LinkEqualityComparer())
                    .ToArray();
                JsonLinksList res = new JsonLinksList(arr);
                Response.Write(JsonTools.MakeJsonString(res));
            }
            catch (Exception ex)
            {
                JsonError err = new JsonError(ex);
                Response.Write(JsonTools.MakeJsonString(err));
            }
        }
    }
}