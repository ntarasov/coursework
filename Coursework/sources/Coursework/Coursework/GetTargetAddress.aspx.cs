﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Coursework.Classes;
using Coursework.Tools;
using Coursework.Tools.JsonTypes;

namespace Coursework
{
    public class GetTargetAddress : Page
    {
        /// <summary>
        /// Метод, вызываемый при обращении к странице
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "application/json";
            string url = Request.Form["url"];
            if (url == null)
            {
                url = Request.QueryString["url"];
                if (url == null)
                {
                    Response.Write("{}");
                    return;
                }
            }
            try
            {
                if (!UrlTools.IsValidAbsoluteWebUrl(url))
                    throw new Exception("Invaid URL");
                using (var client = new RedirectableWebClient())
                {
                    client.DownloadString(url);
                    Response.Write("{\"Target\":\"" + client.ResponseUri + "\"}");
                }
            }
            catch (Exception ex)
            {
                JsonError err = new JsonError(ex);
                Response.Write(JsonTools.MakeJsonString(err));
            }
        }
    }
}