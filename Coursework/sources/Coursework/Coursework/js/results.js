function appendResults(index, data, thread, category)
{
    switch (category)
    {
        case "internal/resource": category = "internalresources"; break;
        case "internal/link": category = "internalhyperlinks"; break;

        case "external/resource":
            category = "externalresources"; break;
        case "external/link" :
            category = "externalhyperlinks"; break;
    }
    var selector = "#resultsDiv_" + category + " > .resultsTable > tbody";
    $(selector).append(makeResultsTableRow(index + 1, data.Url, category, thread));
}

function removeResult(thread) {
    $("#last" + thread).remove();
    console.log("Result row of thread " + thread + " is removed");
}

function completeLastResult(data, thread, category)
{
    $("#pingLoadingGif" + thread).parent().remove();
    for (var i = 0; i < 2; ++i)
        $("#last" + thread + " > td:eq(2)").remove();
    $("#last" + thread).append(makeCompletedCells(data, category));
    updateStats(data);
    $("#last" + thread).attr("class", getRowClassByStatusCode(data.Status))
    $("#last" + thread).removeAttr("id");
}

function makeStatusLine(status) {
    var content = "<span id='status" + status + "'><font class='status' color='";
    content += getColorByStatusCode(status);
    content += "'>" + status + ": </font><font id='statusval" + status + "'>1</font></span><br/>";
    return content;
}

var stats = {}, statsUpdateCallCount = 0, statsFirstCall = true;
function updateStats(data) {
    ++statsUpdateCallCount;
    if (statsFirstCall) {
        statsFirstCall = false;
        $("#processedCount > span:eq(0)").text("");
    }
    var percentage = Math.round((statsUpdateCallCount / urlList.length) * 100);
    $("#processedCount > span:eq(1)").text(percentage + "%");
    if (typeof data.Status == "undefined")
        data.Status = "200";
    if (typeof stats[data.Status] == "undefined")
        stats[data.Status] = 1;
    var selector = "#statusval" + data.Status;
    if ($(selector).length !== 0)
        $(selector).text(++stats[data.Status]);
    else
        $("#stats").append(makeStatusLine(data.Status));
}

function makeResultsTableRow(index, url, category, thread)
{
    var res = "<tr id='last" + thread + "' height='50'>";
    var selector = "#resultsDiv_" + category + " > .resultsTable > tbody > tr";
    res += makeCell("<b>" + ($(selector).length + 1) + "</b>");
    res += makeCell(makeShortLink(url));
    res += makeCell("<img src='/resources/img/ripple.gif' height='45px' id='pingLoadingGif" + thread + "'/>");
    res += makeCell("&nbsp;") + makeCell("&nbsp;");
    res += "</tr>\n";
    return res;
}

function makeCompletedCells(data, category)
{
    // "Status", "ContentType", "StatusText"
    var res = makeCell(data.Status);
    res += makeCell(data.StatusText);
    res += makeCell(data.ContentType);

    if (category.indexOf("resource") != -1) {
        if (data.Length < 1024 * 1024) {
            var len = Math.round((data.Length / 1024) * 100) / 100 + " KB";
        } else
            var len = Math.round((data.Length / (1024 * 1024)) * 100) / 100 + " MB";
        res += makeCell(len);
    }
    return res;
}

function makeCell(text)
{
    return "<td style='max-width: 150px !important; word-wrap: break-word;'>" + text + "</td>";
}

function makeShortLink(url)
{
    return "<a href=\"" + url + "\">" + url + "</a>";
}

function changeResultsTableState(state)
{
    var cssState = "visible";
    if (!state)
        cssState = "hidden";
    $(".resultsTable").css("visibility", cssState);
    $("#resultsDiv_sample > table").css("visibility", "hidden", "!important");
    $("#resultsDiv_sample_size > table").css("visibility", "hidden", "!important");
    $("#resultsDiv_internalresources").css("visibility", cssState);
    $("#resultsDiv_externalresources").css("visibility", cssState);
    $("#resultsDiv_internalhyperlinks").css("visibility", cssState);
    $("#resultsDiv_externalhyperlinks").css("visibility", cssState);
    $(".resultsHeader").css("visibility", cssState);
}