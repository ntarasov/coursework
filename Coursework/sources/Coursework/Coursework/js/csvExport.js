﻿function export_showCsvLink(showingState) {
    var lastInd = 0, content = "";
    if (showingState.ir) {
        content = 'exportCsv("#resultsDiv_internalresources", "Внутренние ресурсы", ' + lastInd + ' )';
        $(".resultsHeader:eq(0)")
            .append(export_makeExportLink(content, lastInd));
        ++lastInd;
    }
    if (showingState.ih) {
        content = 'exportCsv("#resultsDiv_internalhyperlinks", "Внутренние ссылки", ' + lastInd + ' )';
        $(".resultsHeader:eq("+ lastInd + ")")
            .append(export_makeExportLink(content, lastInd));
        ++lastInd;
    }
    if (showingState.er) {
        content = 'exportCsv("#resultsDiv_externalresources", "Внешние ресурсы", ' + lastInd + ')';
        $(".resultsHeader:eq(" + lastInd + ")")
            .append(export_makeExportLink(content, lastInd));
        ++lastInd;
    }
    if (showingState.eh) {
        content = 'exportCsv("#resultsDiv_externalhyperlinks", "Внешние ссылки", ' + lastInd + ')';
        $(".resultsHeader:eq(" + lastInd + ")")
            .append(export_makeExportLink(content, lastInd));
    }
}

function export_makeCsvContent(divSelector) {
    var content = "";
    var selector = divSelector + " > .resultsTable > tbody > tr";
    $(selector).each(function() {
        $(this).children().each(function() {
            content += '"' + $(this).text() + '";';
        });
        content[content.length - 1] = "\r";
        content += '\n';
    });
    return content;
}

function exportCsv(divSelector, header, linkId) {
    var content = export_makeCsvContent(divSelector);
    var fileContent = "data:application/octet-stream," +
        export_encodeString(content);
    var linkSelector = "#export_" + linkId;
    var prevClick = $(linkSelector).attr("onclick");
    $(linkSelector).removeAttr("onclick");
    var website = export_getWebsite(URL);
    $(linkSelector).attr("download", header + " " + website + ".csv");
    $(linkSelector).attr("href", fileContent);
    $(linkSelector).click();
    setTimeout(function() {
        $(linkSelector).removeAttr("download");
        $(linkSelector).removeAttr("href");
        $(linkSelector).attr("onclick", prevClick);
    }, 1000);
}

function export_getWebsite(url)
{
    var website = URL.substr("http://".length);
    if (website[0] == '/')
        website = website.substr(1);
    return website;
}

function export_makeExportLink(onclick, id) {
    return "<br/><font size='-1'><a onclick='" + onclick + "' id='export_" + id +
        "' class='exportLink'>Экспорт в CSV</a></font>";
}

function export_encodeString(str) {
    return encodeURIComponent(str);
}