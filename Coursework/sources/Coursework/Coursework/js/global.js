﻿var threads = 5;

var loaded = false;
var pause = true;
var lastProcessed = -1;
var urlList;
var getLinkResult;
var URL;
var headOnly = false;

function getColorByStatusCode(status){
    if (status < 300)
        return "#0C0";
    if (status < 400)
        return "#CC0";
    return "#C00";
}

function getRowClassByStatusCode(status) {
    if (status >= 300 && status < 400)
        return "warning";
    if (status >= 400)
        return "danger";
    return "success";
}