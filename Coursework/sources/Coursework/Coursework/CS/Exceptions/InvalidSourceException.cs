﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Coursework.Classes
{
    public class InvalidSourceException : Exception
    {
        public InvalidSourceException() : base("Invalid source file is given")
        {
            return;
        }

        public InvalidSourceException(string message) : base(message)
        {
            return;
        }
    }
}