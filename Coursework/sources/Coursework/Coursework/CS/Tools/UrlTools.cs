﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using Coursework.Classes;

namespace Coursework.Tools
{
    public class UrlTools
    {
        /// <summary>
        /// Тип для метода, проверяющего корректность найденной ссылки
        /// </summary>
        /// <param name="source">Исходный код страницы</param>
        /// <param name="linkPosition">Позиция начала ссылки</param>
        /// <returns>Корректность ссылки</returns>
        public delegate bool UrlValidator(string source, int linkPosition);
        public delegate void Action();

        private readonly Uri sourcePageUri;

        public UrlTools(Uri sourcePageUri)
        {
            this.sourcePageUri = sourcePageUri;
        }

        /// <summary>
        /// Получает URL между preTag и stopSymbol
        /// </summary>
        /// <param name="preTag">Подстрока, предшествующая URL</param>
        /// <param name="stopSymbols">Символы, возможно идущие после ссылки</param>
        /// <param name="validator">Корректен ли найденный URL</param>
        /// <param name="isHtml">HTML формат исходного кода</param>
        /// <returns>Массив найденных Uri</returns>
        public Link[] ParseUrlBetween(Uri baseUri,
            string source, 
            string preTag, 
            string stopSymbols, 
            UrlValidator validator, 
            bool isHtml)
        {
            int lastPosition = -1;
            List<Link> uriList = new List<Link>();

            while (lastPosition < source.Length)
            {
                lastPosition = source.IndexOf(preTag, lastPosition + 1);
                if (lastPosition == -1)
                    break; // Not found
                int srcPosition = source.IndexOf(preTag, lastPosition);
                if (srcPosition == -1)
                    break;
                srcPosition += preTag.Length;
                if (IsUrlPreventingSymbol(source[srcPosition]))
                {
                    ++srcPosition;
                    if (srcPosition >= source.Length)
                        throw new InvalidSourceException();
                }
                StringBuilder linkBuidler = new StringBuilder();
                while (stopSymbols.IndexOf(source[srcPosition]) == -1)
                {
                    linkBuidler.Append(source[srcPosition]);
                    ++srcPosition;
                    if (srcPosition >= source.Length)
                        throw new InvalidSourceException();
                }
                string link = linkBuidler.ToString();
                Uri linkUri;
                try
                {
                    if (!validator(source, lastPosition))
                        continue;
                    if (IsValidAbsoluteWebUrl(link))
                        linkUri = new Uri(link);
                    else
                    {
                        linkUri = new Uri(baseUri, link);
                        if (!IsValidAbsoluteWebUrl(linkUri.AbsoluteUri))
                            continue;
                    }
                    // Check if link is hyperlink or resource
                    int pos;
                    string category = GetResourceCategory(linkUri, sourcePageUri);
                    if (isHtml)
                    {
                        for (pos = lastPosition; pos > 0 && source[pos] != '<'; --pos) ;
                        --pos;
                        if (source.IndexOf("<a", pos) == pos + 1)
                            category = GetLinkCategory(linkUri, sourcePageUri);
                    }
                    uriList.Add(new Link(linkUri.AbsoluteUri, category));
                }
                catch (Exception) { /* Ignore */ }
            }
            return uriList.Distinct().ToArray();
        }
        /// <summary>
        /// Определеяет категорию гиперссылки
        /// </summary>
        /// <param name="link">Гиперссылка</param>
        /// <param name="baseUri">Cсылка на исходную страницу</param>
        private static string GetLinkCategory(Uri link, Uri baseUri)
        {
            return link.Host != baseUri.Host
                ? LinkCategory.ExternalHyperlink
                : LinkCategory.InternalHyperlink;
        }

        /// <summary>
        /// Определяет категорию ресурса
        /// </summary>
        /// <param name="link">Ссылка на ресурс</param>
        /// <param name="baseUri">Ссылка на исходную страницу</param>
        private static string  GetResourceCategory(Uri link, Uri baseUri)
        {
            return link.Host != baseUri.Host
                ? LinkCategory.ExternalResource
                : LinkCategory.InternalResource;
        }

        /// <summary>
        /// Является ли предъявленный символ предшествующим ссылке
        /// </summary>
        public static bool IsUrlPreventingSymbol(char symb)
        {
            return symb == '"' || symb == '\'' || symb == ' ';
        }

        /// <summary>
        /// Получает директорию файла или возвращает адрес директории
        /// </summary>
        public static string GetAbsoluteDir(Uri uri)
        {
            StringBuilder result = new StringBuilder(uri.AbsoluteUri);
            if (result.Length == 0)
                throw new ArgumentException("Argument URI was in wrong format");
            if (!uri.IsFile)
                return uri.AbsoluteUri;
            while (result[result.Length - 1] != '/')
            {
                result.Remove(result.Length - 1, 1); // Pop back
                if (result.Length <= ("http://").Length + 1)
                    return uri.AbsoluteUri + "/";
            }
            return result.ToString();
        }

        public static bool IsValidAbsoluteWebUrl(string url)
        {
            return url.IndexOf("http") == 0 &&
                Uri.IsWellFormedUriString(url, UriKind.Absolute);
            // Если ссылка начинается не с http то либо используется другой протокол
        }

        public static bool IsValidRelativeWebUrl(string url)
        {
            return Uri.IsWellFormedUriString(url, UriKind.Relative);
        }
    }
}