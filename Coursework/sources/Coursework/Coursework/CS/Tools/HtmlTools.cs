﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Coursework.Tools
{
    public static class HtmlTools
    {
        /// <summary>
        /// Аттрибуты HTML тегов, предшествующие ссылке
        /// </summary>
        public static readonly string[] UrlAttributes = {
            "action", "background", "classid", "codebase", "data", "href",
            "longdesc", "profile", "src", "usemap" };
    }
}