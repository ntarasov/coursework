﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Coursework.Tools
{
    public static class JsonTools
    {
        /// <summary>
        /// Составляет строку-JSON объект
        /// </summary>
        /// <typeparam name="JsonObject">Тип, представляющий шаблон для объекта</typeparam>
        /// <param name="obj">Экземпляр</param>
        public static string MakeJsonString<JsonObject>(JsonObject obj)
        {
            string result;
            using (MemoryStream stream = new MemoryStream())
            {
                var serializer = new DataContractJsonSerializer(typeof(JsonObject));
                serializer.WriteObject(stream, obj);
                stream.Position = 0;
                using (StreamReader reader = new StreamReader(stream))
                    result = reader.ReadToEnd();
            }
            return result.Replace("\\", "");
        }
    }
}