﻿using System;
using System.Runtime.Serialization;

namespace Coursework.Tools.JsonTypes
{
    [DataContract]
    public class JsonError
    {
        [DataMember(Name = "Error")]
        public string Message { get; set; }

        public JsonError(Exception e)
        {
            Message = e.Message;
        }
    }
}