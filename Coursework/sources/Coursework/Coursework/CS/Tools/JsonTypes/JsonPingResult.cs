﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Coursework.Classes;
using Coursework.Classes.Ping;

namespace Coursework.Tools.JsonTypes
{
    [DataContract]
    public class JsonPingResult
    {
        [DataMember(Name = "Status")]
        public string NumericStatus { get; set; }

        [DataMember(Name = "ContentType")]
        public string ContentType { get; set; }

        [DataMember(Name = "StatusText")]
        public string TextStatus { get; set; }

        [DataMember(Name = "Length")]
        public string Length { get; set; }

        public JsonPingResult(PingResponse response)
        {
            TextStatus = response.Status.ToString();
            NumericStatus = ((int)response.Status).ToString();
            ContentType = response.ContentType;
            Length = response.FileSize.ToString();
        }

    }
}