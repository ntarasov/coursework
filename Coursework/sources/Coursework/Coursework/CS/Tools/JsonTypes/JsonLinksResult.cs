﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Coursework.Classes;

namespace Coursework.Tools.JsonTypes
{
    [DataContract]
    public class JsonLinksList
    {
        [DataMember(Name = "Result")]
        public List<Link> UrlList { get; set; }

        public JsonLinksList(Link[] linkArr)
        {
            UrlList = new List<Link>(linkArr);
        }
    }
}