﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Coursework.Classes
{
    public static class HttpWebRequestExtension
    {
        /// <summary>
        /// Получает Http ответ с любым статусом без выбрасывания исключения
        /// </summary>
        public static HttpWebResponse GetResponseNoException(this HttpWebRequest request)
        {
            HttpWebResponse response;
            try
            {
                request.AllowAutoRedirect = false;
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                response = e.Response as HttpWebResponse;
                if (response == null)
                    throw;
            }
            return response;
        }
    }
}