﻿using System;
using System.Runtime.Serialization;

namespace Coursework.Classes
{
    [DataContract]
    public struct Link
    {
        [DataMember(Name = "Url")]
        public string Url { get; private set; }

        [DataMember(Name = "Category")]
        public string Category { get; private set; }

        public Link(string url, string category)
        {
            Url = url;
            Category = category;
        }
    }
}