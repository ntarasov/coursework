﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Coursework.Classes
{
    public class LinkEqualityComparer : IEqualityComparer<Link>
    {
        /// <summary>
        /// Сравнивает ссылки по значению
        /// </summary>
        public bool Equals(Link a, Link b)
        {
            return a.Url == b.Url;
        }

        public int GetHashCode(Link link)
        {
            return link.Url.GetHashCode();
        }
    }
}