﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace Coursework.Classes
{
    class RedirectableWebClient : WebClient
    {
        public RedirectableWebClient()
        {
            string agent = Globals.UserAgent;
            Headers.Add(HttpRequestHeader.UserAgent, agent);
        }

        public Uri ResponseUri { get; private set; }

        protected override WebResponse GetWebResponse(WebRequest request)
        {
            WebResponse response = base.GetWebResponse(request);
            ResponseUri = response.ResponseUri;
            return response;
        }
    }
}