﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using Coursework.Tools;

namespace Coursework.Classes
{
    public class WebPage
    {
        public string Content { get; protected set; }
        public string Url { get; protected set; }
        public Uri Uri { get; protected set; } 

        public WebPage(string url)
        {
            if (!UrlTools.IsValidAbsoluteWebUrl(url))
                throw new ArgumentException("Invalid URL given");
            this.Url = url;
            Uri = new Uri(url);
        }

        public WebPage(Uri uri)
        {
            if (!UrlTools.IsValidAbsoluteWebUrl(uri.AbsoluteUri))
                throw new ArgumentException("Invalid URL given");
            this.Uri = uri;
            Url = uri.AbsoluteUri;
        }

        /// <summary>
        /// Загружает содержимое страницы
        /// </summary>
        public virtual void Download()
        {
            using (var wc = new RedirectableWebClient())
            {
                Content = wc.DownloadString(Url);
                Uri = wc.ResponseUri;
                Url = wc.ResponseUri.AbsolutePath;
            }
        }
    }
}