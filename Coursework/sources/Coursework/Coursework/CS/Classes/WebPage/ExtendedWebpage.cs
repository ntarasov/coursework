﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Net;
using Coursework.Tools;

namespace Coursework.Classes
{
    public class ExtendedWebPage : WebPage
    {
        private UrlTools urlTools;
        private IList<string> visitedCssList; // *

        public ExtendedWebPage(string url) : base(url)
        {
            urlTools = new UrlTools(Uri);
        }

        public override void Download()
        {
            base.Download();
            urlTools = new UrlTools(Uri);
        }
        /// <summary>
        /// Получает массив ссылок с исходной страницы
        /// и с прикрепленных таблиц стилей
        /// </summary>
        public Link[] ParseLinks()
        {
            if (Content == null)
                throw new Exception("Webpage is not loaded");
            var result = new List<Link>();
            Uri[] cssSources = GetCssSourceAdress();
            visitedCssList = new List<string>();
            foreach (Uri cssSource in cssSources)
                result.AddRange(GetUrlFromCss(cssSource));
            result.AddRange(GetUrlFromHtml(Content));
            return result.ToArray();
        }

        /// <summary>
        /// Получает список ссылок из CSS и вложенных таблиц стилей рекурсивно
        /// </summary>
        /// <param name="source">Адрес страницы CSS стиля</param>
        private Link[] GetUrlFromCss (Uri source)
        {
            try
            {
                visitedCssList.Add(source.AbsolutePath); // *
                WebPage wp = new WebPage(source);
                wp.Download();

                var result = new List<Link>();
                Uri baseUri = new Uri(UrlTools.GetAbsoluteDir(source));
                UrlTools.UrlValidator alwaysValid = (src, pos) => true;

                Link[] addRange = urlTools.ParseUrlBetween(baseUri, wp.Content, 
                    "url(", "' \")", alwaysValid, false);
                result.AddRange(addRange);

                int resultOldSize = result.Count;
                for (int i = 0; i < resultOldSize; ++i)
                    if (result[i].Url.IndexOf(".css") != -1 &&
                        visitedCssList.IndexOf(result[i].Url) == -1) // *
                    {
                        Link[] range = GetUrlFromCss(new Uri(result[i].Url));
                        result.AddRange(range);
                    }
                for (int i = 0; i < result.Count; ++i)
                {
                    if (!UrlTools.IsValidAbsoluteWebUrl(result[i].Url))
                    {
                        result.RemoveAt(i);
                        --i;
                    }
                }
                return result.Distinct().ToArray();
            }
            catch (Exception)
            {
                return new Link[0];
            }
        }

        /// <summary>
        /// Получает массив ссылок из HTML исходного кода и встроенных CSS
        /// </summary>
        private Link[] GetUrlFromHtml (string code)
        {
            List<Link> result = new List<Link>();

            UrlTools.UrlValidator validator = (src, pos) =>
            {
                int linkPos = pos;
                while (linkPos - pos < 128 && src[pos] != '<' && pos >= 1)
                {
                    if (src[pos] == '>' || 
                    ((src[pos] == '"' || src[pos] == '\'') &&  src[pos - 1] == '='))
                        return false;
                    --pos;
                }
                return pos != -1 && IsNotHtmlDataAttr(src, linkPos);
            };
            code = EraseScripts(code);

            Uri baseUri = new Uri(UrlTools.GetAbsoluteDir(Uri));
            // HTML Links :
            foreach (string attr in HtmlTools.UrlAttributes)
            {
                Link[] toMerge = urlTools.ParseUrlBetween(baseUri, code, attr + "=",
                    "'\" >", validator, true);
                result.AddRange(toMerge);
            }
            // CSS into HTML :
            validator = (src, pos) => src.IndexOf("</", pos) == src.IndexOf("</style");
            Link[] css = urlTools.ParseUrlBetween(baseUri, code, "url(", ")'\"", validator, false);
            result.AddRange(css);
            return result.Distinct().ToArray();
        }

        /// <summary>
        /// Проверяет, не является ли ссылка на позиции значением data-* атрибута
        /// </summary>
        private bool IsNotHtmlDataAttr(string htmlSource, int linkPos)
        {
            string dataActionPrefString = "data-";

            for (var counter = 0; 
                counter < 16 && htmlSource[--linkPos] != '-'; 
                ++counter);

            int len = dataActionPrefString.Length;
            for (var i = 0; i < len; ++i)
            {
                if (htmlSource[linkPos--] != dataActionPrefString[len - i - 1])
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Проверяет, экспортирован ли скрипт из внешнего файла
        /// </summary>
        private bool IsScriptExported(string htmlSource, int beginPos)
        {
            int openingTagEndingPos = htmlSource.IndexOf('>', beginPos);
            int exportAttrPos = htmlSource.IndexOf("src=", beginPos);
            return exportAttrPos != -1 && exportAttrPos < openingTagEndingPos;
        }

        /// <summary>
        /// Проверяет, находится ли символ на позиции begin в списке на удаление
        /// </summary>
        private bool IsAlreadyRemoved(IEnumerable<Tuple<int, int>> toRemove, int begin) => 
            toRemove.Any(t => begin < t.Item2 && begin > t.Item1);

        /// <summary>
        /// Удаляет скрипты из исходного кода страницы
        /// </summary>
        private string EraseScripts(string code)
        {
            int beginPos = 0;
            var toRemove = new List<Tuple<int, int>>();

            while ((beginPos = code.IndexOf("<script", beginPos + 1)) != -1)
            {
                if (IsScriptExported(code, beginPos))
                    continue;
                int endPos = code.IndexOf("</script>", beginPos + 1);
                if (endPos == -1)
                    throw new InvalidSourceException();
                endPos += "</script>".Length;
                if (!IsAlreadyRemoved(toRemove, beginPos))
                    toRemove.Add(new Tuple<int, int>(beginPos, endPos));
            }
            StringBuilder formattedSource = new StringBuilder();
            int insertBeginPos = 0;
            
            for (int i = 0; i < toRemove.Count; ++i)
            {
                formattedSource.Append(code
                    .Substring(insertBeginPos, toRemove[i].Item1 - insertBeginPos));
                insertBeginPos = toRemove[i].Item2;
            }
            formattedSource.Append(code.Substring(insertBeginPos));
            return formattedSource.ToString();
        }

        /// <summary>
        /// Получает массив ссылок на импортированные таблицы стилей в HTML
        /// </summary>
        private Uri[] GetCssSourceAdress()
        {
            string source = Content;
            int lastPosition = -1;
            var uriList = new List<Uri>();

            while (lastPosition < source.Length)
            {
                lastPosition = source.IndexOf("<link", lastPosition + 1);
                if (lastPosition == -1)
                    break; // Not found
                int nearestCloseBracket = lastPosition;
                while (source[nearestCloseBracket] != '>')
                {
                    ++nearestCloseBracket;
                    if (nearestCloseBracket >= source.Length)
                        throw new InvalidSourceException();
                }
                int typeTagPos = source.IndexOf("stylesheet", lastPosition);
                if (typeTagPos == -1 || typeTagPos > nearestCloseBracket)
                {
                    ++lastPosition;
                    continue;
                }
                int srcPosition = source.IndexOf("href=", lastPosition);
                if (srcPosition == -1 || srcPosition > nearestCloseBracket)
                {
                    ++lastPosition;
                    continue;
                }
                srcPosition += ("href=").Length;
                while (UrlTools.IsUrlPreventingSymbol(source[srcPosition]))
                {
                    ++srcPosition;
                    if (srcPosition >= source.Length)
                        throw new InvalidSourceException();
                }
                StringBuilder linkBuidler = new StringBuilder();
                while (source[srcPosition] != '"' &&
                        source[srcPosition] != '\'' &&
                        source[srcPosition] != ' ')
                {
                    linkBuidler.Append(source[srcPosition]);
                    ++srcPosition;
                    if (srcPosition >= source.Length)
                        throw new InvalidSourceException();
                }
                string link = linkBuidler.ToString();
                Uri linkUri;
                if (UrlTools.IsValidAbsoluteWebUrl(link))
                    linkUri = new Uri(link);
                else
                    linkUri = new Uri(Uri, link);
                uriList.Add(linkUri);
            }
            return uriList.ToArray();
        }
    }
}