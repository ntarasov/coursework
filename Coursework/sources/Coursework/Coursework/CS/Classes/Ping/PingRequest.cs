﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using Coursework.Tools;

namespace Coursework.Classes.Ping
{
    public class PingRequest
    {
        private Uri target;

        public PingRequest(Uri uri)
        {
            if (!UrlTools.IsValidAbsoluteWebUrl(uri.AbsoluteUri))
                throw new ArgumentException("Invalid URI");
            target = uri;
        }

        public PingRequest(string url)
        {
            if (!UrlTools.IsValidAbsoluteWebUrl(url))
                throw new ArgumentException("Invalid URI");
            target = new Uri(url);
        }

        /// <summary>
        /// Опрашивает ресурс
        /// </summary>
        /// <param name="headOnly">Послать запрос HEAD</param>
        public PingResponse Ping(bool headOnly)
        {
            var request = (HttpWebRequest)WebRequest.Create(target);
            if (headOnly)
                request.Method = "HEAD";
            request.UserAgent = Globals.UserAgent;
            PingResponse result;
            using (var response = request.GetResponseNoException())
            {
                result = new PingResponse
                {
                    ContentType = response.ContentType,
                    Status = response.StatusCode,
                    FileSize = response.ContentLength
                };
            }
            return result;
        }
    }
}