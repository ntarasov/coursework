﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

namespace Coursework.Classes.Ping
{
    public struct PingResponse
    {
        public string ContentType { get; set; }
        public HttpStatusCode Status { get; set; }
        public long FileSize { get; set; }
    }
}