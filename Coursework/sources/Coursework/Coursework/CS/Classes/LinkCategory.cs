﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Coursework.Classes
{
    /// <summary>
    /// Содержит строковые представления категорий ссылок
    /// </summary>
    public static class LinkCategory
    {
        public const string InternalResource = "internal/resource";
        public const string InternalHyperlink = "internal/link";

        public const string ExternalResource = "external/resource";
        public const string ExternalHyperlink = "external/link";
    }
}