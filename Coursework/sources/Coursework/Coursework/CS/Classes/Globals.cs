﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Coursework.Classes
{
    public class Globals
    {
        /// <summary>
        /// UserAgent сервиса
        /// </summary>
        public const string UserAgent =
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.70 Safari/533.4";
    }
}